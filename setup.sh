#!/bin/bash
# -*- coding: utf-8 -*-
set -e
export DEBIAN_FRONTEND=noninteractive

users=$(getent passwd | awk -F: '($7 ~ /(bash|zsh|sh)/ && $6 ~ /^\/home\//) {print $1}')

apt-get update -yqq
apt-get install -yqq \
    curl \
    libc++1 \
    libgconf-2-4 \
    gpg \
    apt-transport-https \
    lsb-release \
    ca-certificates \
    software-properties-common \
    libxcb-xtest0 \
    ibus \
    libgl1-mesa-glx \
    libappindicator1 \
    build-essential \
    libgtkmm-3.0-1v5

curl -sSL https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --dearmor -o /etc/apt/trusted.gpg.d/vscode.gpg

cat > /etc/apt/sources.list.d/extra.list << EOF
deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/vscode.gpg]             https://packages.microsoft.com/repos/code stable main
EOF

apt-get update -yqq
apt-get install -yqq code 

(
    cd /tmp;
    curl -sSLOJ $(curl -sSL 'https://www.sublimetext.com/download_thanks?target=x64-deb' | grep -Eoi '\"https://.*.deb\"' | tr -d '"' | head -1)
    curl -sSLOJ https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    curl -sSLOJ https://zoom.us/client/latest/zoom_amd64.deb
    curl -sSLOJ "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"
    dpkg -i *.deb 
    rm -rf /tmp/*.deb

    curl -sSL https://softwareupdate.vmware.com/cds/vmw-desktop/ws/17.6.0/24238078/linux/core/VMware-Workstation-17.6.0-24238078.x86_64.bundle.tar -o vmware.tar
    tar xf vmware.tar
    mv *.bundle vmware.bundle
    /bin/bash ./vmware.bundle

    version_17=(MC60H-DWHD5-H80U9-6V85M-8280D 4A4RR-813DK-M81A9-4U35H-06KND NZ4RR-FTK5H-H81C1-Q30QH-1V2LA JU090-6039P-08409-8J0QH-2YR7F 4Y09U-AJK97-089Z0-A3054-83KLA 4C21U-2KK9Q-M8130-4V2QH-CF810)
    random_key=${version_17[$RANDOM%${#version_17[@]}]}
    version=$(vmware -v | grep -Eo "([0-9]{1,}\.)+[0-9]{1,}")
    /usr/lib/vmware/bin/licenseTool enter "${random_key}" "" "" "${version}" "VMware Workstation" /usr/lib/vmware
)

curl -fsSL https://get.docker.com -o - | bash
curl -sSkL $(curl -sKL https://api.github.com/repos/docker/compose/releases | grep -i "$(uname -s)-$(uname -m)" | grep -i 'browser_download_url' | grep -v 'sha256' | head -1 | awk -F'"' '{print $4}') -o - >/usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
usermod -aG docker $USER

if [[ $XDG_DATA_DIRS = *plasma* ]] && [[ $(printf $XDG_SESSION_DESKTOP | tr '[[:upper:]]' '[[:lower:]]') = *kde* ]]; then
    apt-get update -yqq
    apt-get install -yqq \
        kompare \
        okteta \
        ark \
        filelight \
        kfind \
        kwrite \
        sweeper \
        kdiff3 \
        partitionmanager \
        okular \
        kolourpaint
fi

for user in $users; do
    mkdir -p "/home/${user}/.chrome-profiles"
    mkdir -p "/home/${user}/.config/Code/User"
    cat > "/home/${user}/.config/Code/User/settings.json" << EOF
{
    "workbench.startupEditor": "none",
    "workbench.enableExperiments": false,
    "update.mode": "manual",
    "editor.cursorBlinking": "smooth",
    "editor.cursorStyle": "underline",
    "editor.minimap.enabled": false,
    "editor.acceptSuggestionOnCommitCharacter": false,
    "editor.acceptSuggestionOnEnter": "off",
    "workbench.commandPalette.history": 0,
    "workbench.editor.restoreViewState": false,
    "workbench.settings.enableNaturalLanguageSearch": false,
    "window.menuBarVisibility": "toggle",
    "window.restoreWindows": "none",
    "terminal.integrated.cursorStyle": "underline",
    "terminal.integrated.cursorBlinking": true,
    "telemetry.enableCrashReporter": false,
    "update.showReleaseNotes": false,
    "telemetry.enableTelemetry": false,
    "npm.fetchOnlinePackageInfo": false,
    "workbench.sideBar.location": "right",
    "editor.suggestSelection": "first",
    "vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue",
    "shellcheck.disableVersionCheck": true,
    "workbench.colorTheme": "Breeze Dark",
    "extensions.ignoreRecommendations": true,
}
EOF

    chown -R "${user}:${user}" "/home/${user}"
done

cat > /usr/share/applications/applications-browser.desktop << "EOF"
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Name=Applications Browser
Terminal=false
StartupNotify=true
Type=Application
Exec=/usr/bin/google-chrome-stable %U --class=applications-browser
Icon=applications-browser
StartupWMClass=applications-browser
Actions=microsoft-profile;isolated-microsoft-profile;discord-profile;isolated-discord-profile;slack-profile;isolated-slack-profile;incognito;

[Desktop Action microsoft-profile]
Name=Microsoft Profile
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --disable-fre --no-default-browser-check --no-first-run --user-data-dir=$HOME/.chrome-profiles/microsoft --profile-directory=Default https://portal.office.com'

[Desktop Action isolated-microsoft-profile]
Name=Isolated Microsoft Profile
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --disable-fre --no-default-browser-check --no-first-run --user-data-dir=$(mktemp -d) --profile-directory=Default https://portal.office.com'

[Desktop Action discord-profile]
Name=Discord Profile
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --disable-fre --no-default-browser-check --no-first-run --user-data-dir=$HOME/.chrome-profiles/discord --profile-directory=Default https://discord.com'

[Desktop Action isolated-discord-profile]
Name=Isolated Discord Profile
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --disable-fre --no-default-browser-check --no-first-run --user-data-dir=$(mktemp -d) --profile-directory=Default https://discord.com'

[Desktop Action slack-profile]
Name=Slack Profile
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --disable-fre --no-default-browser-check --no-first-run --user-data-dir=$HOME/.chrome-profiles/discord --profile-directory=Default https://app.slack.com'

[Desktop Action isolated-slack-profile]
Name=Isolated Slack Profile
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --disable-fre --no-default-browser-check --no-first-run --user-data-dir=$(mktemp -d) --profile-directory=Default https://app.slack.com'

[Desktop Action incognito]
Name=Incognito
Exec=/bin/sh -c '$(which google-chrome-stable) --class=applications-browser --incognito https://google.com'
EOF

#function _sf-fonts() {
#    install_dependencies
#    tmpdir=$(mktemp -d)
#    cd $tmpdir
#    mkdir -p $HOME/.fonts
#
#    dmgs=(
#        "https://devimages-cdn.apple.com/design/resources/download/SF-Pro.dmg"
#        "https://devimages-cdn.apple.com/design/resources/download/SF-Compact.dmg"
#        "https://devimages-cdn.apple.com/design/resources/download/SF-Mono.dmg"
#        "https://devimages-cdn.apple.com/design/resources/download/NY.dmg"
#    )
#    for dmg in "${dmgs[@]}"; do
#        name=$(basename "${dmg}")
#        printf "Downloading $dmg\n"
#        curl -sSkL $dmg -o $tmpdir/$name
#    done
#
#    printf "Unpacking Apple .dmg files...\n"
#    for each in *.dmg; do 7z e $each "*.pkg" -r >/dev/null 2>&1; done
#    for each in *.pkg; do 7z x "$each" -o"${each}.d" >/dev/null 2>&1; done
#    for each in *.d; do 7z x "${each}/*" >/dev/null 2>&1; done
#
#    printf "Installing SF fonts user and system-wide\n"
#    cp -r Library/Fonts/*.otf $HOME/.fonts
#    sudo mkdir -p /usr/share/fonts/opentype/SF && sudo cp -r Library/Fonts/*.otf /usr/share/fonts/opentype/SF
#
#    printf "Rebuilding font cache\n"
#    sudo fc-cache -f
#}

function _virtualbox() {
    install_dependencies
    LATEST_STABLE=$(curl -sSL https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT)
    URL=$(curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}")
    . /etc/os-release
    UBUNTU_DEB=$(echo "$URL" | grep -i "${UBUNTU_CODENAME}" | head -1 | grep -Eoi "\".*_amd64.deb\"" | tr -d '"')
    curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}/${UBUNTU_DEB}" -o /tmp/virtualbox_amd64.deb
    sudo dpkg -i /tmp/virtualbox_amd64.deb
    rm -rf /tmp/virtualbox_amd64.deb

    EXT_PACK=$(echo "$URL" | grep -Eoi "\".*.vbox-extpack\"" | head -1 | tr -d '"') 
    curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}/${EXT_PACK}" -o "/tmp/${EXT_PACK}"
    echo "yes" | sudo VBoxManage extpack install "/tmp/${EXT_PACK}"
    rm -rf "/tmp/${EXT_PACK}"
}

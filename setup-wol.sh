#!/bin/bash
# -*- coding: utf-8 -*-

[[ $EUID -ne 0 ]] && printf "%s\n" "run as root" && exit 1
[[ ! $(command -v ethtool) ]] && printf "%s\n" "ethtool not found, exiting" && exit 1
[[ -z $1 ]] && printf "%s\n" "usage: $(basename $0) <iface>" && exit 1

cat > /etc/systemd/system/wol@.service << EOF
[Unit]
Description=Wake-on-LAN for %i

[Service]
Type=oneshot
ExecStartPre=$(which sleep) 30
ExecStart=$(which ethtool) --change %i wol g

[Install]
WantedBy=network.target
EOF

systemctl daemon-reload
systemctl enable --now wol@$1.service

#!/bin/bash

if ! command -v xdotool &>/dev/null; then
    printf "%s\n" "xdotool not found, exiting"
    exit 1
fi

usage() {
    printf "%s\n" \
        "auto clicker" \
        "" \
        "-x     x coordinates" \
        "-y     y coordinates" \
        "-s     seconds to sleep between clicks" \
        "-c     how many clicks" \
        "-j     jitter" \
        "-a     auto use mouse coordinates" \
        "-g     get current mouse coordinates" \
        "-r     restore windows focus" \
        "" \
        "examples:" \
        "$(basename $0) -g" \
        "$(basename $0) -x 1234 -y 5678 -s 3.5" \
        "$(basename $0) -a -r -s 30 -j 3"
    exit 1
}

clicks="1"
jitter="1.5"

while getopts "x:y:s:c:j:agrh" opt; do
    case $opt in
        x) x_coordinates="${OPTARG}";;
        y) y_coordinates="${OPTARG}";;
        s) sleep_timer="${OPTARG}";;
        c) clicks="${OPTARG}";;
        j) jitter="${OPTARG}";;
        a) action="auto";;
        g) get_coordinates=true;;
        r) restore=true;;
        h) usage;;
    esac
done

if [[ -n $x_coordinates && -n $y_coordinates && -n $sleep_timer ]]; then
    while :; do
        current_window=$(xdotool getactivewindow)
        random_sleep=$(awk -v min=-$jitter -v max=$jitter -v val="$sleep_timer" 'BEGIN{srand(); printf "%.1f\n", val + min + rand() * (max - min)}')
        if [[ $restore == true ]]; then
            printf "%s\n" "clicking ${x_coordinates}x${y_coordinates} (${clicks} times) and restoring, sleeping for ${random_sleep}"
            for i in $(seq 1 $clicks); do xdotool mousemove $x_coordinates $y_coordinates click 1 sleep 0.01 mousemove restore; done
        else
            printf "%s\n" "clicking ${x_coordinates}x${y_coordinates} (${clicks} times), sleeping for ${random_sleep}"
            for i in $(seq 1 $clicks); do xdotool mousemove $x_coordinates $y_coordinates click 1 sleep 0.01; done
        fi
        xdotool windowactivate $current_window
        sleep $random_sleep
    done
elif [[ $action == "auto" && -n $sleep_timer ]]; then
    coordinates=$(xdotool getmouselocation --shell)
    x_coordinates=$(echo "$coordinates" | grep -oP '(?<=X=)\d+')
    y_coordinates=$(echo "$coordinates" | grep -oP '(?<=Y=)\d+')

    while :; do
        current_window=$(xdotool getactivewindow)
        random_sleep=$(awk -v min=-$jitter -v max=$jitter -v val="$sleep_timer" 'BEGIN{srand(); printf "%.1f\n", val + min + rand() * (max - min)}')
        if [[ $restore == true ]]; then
            printf "%s\n" "clicking ${x_coordinates}x${y_coordinates} (${clicks} times) and restoring, sleeping for ${random_sleep}"
            for i in $(seq 1 $clicks); do xdotool mousemove $x_coordinates $y_coordinates click 1 sleep 0.01 mousemove restore; done
        else
            printf "%s\n" "clicking ${x_coordinates}x${y_coordinates} (${clicks} times), sleeping for ${random_sleep}"
            for i in $(seq 1 $clicks); do xdotool mousemove $x_coordinates $y_coordinates click 1 sleep 0.01; done
        fi
        xdotool windowactivate $current_window
        sleep $random_sleep
    done
else
    if [[ $get_coordinates == true ]]; then
        coordinates=$(xdotool getmouselocation --shell)
        printf "%s\n" $coordinates
    else
        usage
    fi
fi

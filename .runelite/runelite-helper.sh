#!/bin/bash

usage() {
    printf "%s\n" \
        "small runelite/osrs helper" \
        "usage: $(basename $0) -d|-a|-h" \
        "" \
        "-d     download latest runelite launcher" \
        "-a     perform oauth authentication flow to generate credentials.properties" \
        "-h     print this help message and exit"
    exit 1
}

download() {
    if ! command -v curl &>/dev/null; then
        printf "%s\n" "curl not found"
        exit 1
    fi

    latest_appimage=$(curl -sL https://api.github.com/repos/runelite/launcher/releases | grep -i browser_download_url | grep -Ei '.*appimage' | cut -d '"' -f4 | grep -v aarch64 | head -1)
    latest_jar=$(curl -sL https://api.github.com/repos/runelite/launcher/releases | grep -i browser_download_url | grep -Ei '.*jar' | cut -d '"' -f4 | grep -v aarch64 | head -1)

    if test -z $latest; then
        printf "%s\n" "[ERR] could not find latest runelite release"
        exit 1
    else
        printf "%s\n" "[INFO] found latest runelite release: ${latest}"
    fi

    mkdir -p "${HOME}/.runelite" &>/dev/null
    cp default.properties "${HOME}/.runelite/default.properties"

    cd "${HOME}/.runelite"
    if curl -s -L -O -J $latest_appimage; then
        printf "%s\n" "[INFO] successfully downloaded ${latest_appimage}"
    else
        printf "%s\n" "[ERR] failed to download ${latest_appimage}"
        exit 1
    fi
    chmod +x $HOME/.runelite/*AppImage

    if curl -s -L -O -J $latest_jar; then
        printf "%s\n" "[INFO] successfully downloaded ${latest_jar}"
    else
        printf "%s\n" "[ERR] failed to download ${latest_jar}"
        exit 1
    fi

    cat > RuneLite.desktop << EOF
[Desktop Entry]
Version=1.0
Name=RuneLite
Terminal=false
StartupNotify=true
Type=Application
Exec=${HOME}/.runelite/RuneLite.AppImage
Icon=${HOME}/.runelite/icon.png
StartupWMClass=RuneLite
EOF

    printf "%s\n" "[INFO] created desktop shortcut \"~/.runelite/RuneLite.desktop\""
    cd - &>/dev/null
}

authenticate() {
    if ! command -v openssl &>/dev/null; then
        printf "%s\n" "[ERR] openssl not found"
        exit 1
    fi

    if ! command -v curl &>/dev/null; then
        printf "%s\n" "[ERR] curl not found"
        exit 1
    fi

    if ! command -v jq &>/dev/null; then
        printf "%s\n" "[ERR] jq not found"
        exit 1
    fi

    code_verifier=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c 45)
    code_challenge=$(echo -n "$code_verifier" | openssl dgst -sha256 -binary | openssl enc -base64 | tr '+/' '-_' | tr -d '=')
    state=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c 8)
    nonce=$(tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c 48)

    cat << EOF
[INFO] Browse to this URL, authenticate and paste the constructed URL

https://account.jagex.com/oauth2/auth?auth_method=&login_type=&flow=launcher&response_type=code&client_id=com_jagex_auth_desktop_launcher&redirect_uri=https%3A%2F%2Fsecure.runescape.com%2Fm%3Dweblogin%2Flauncher-redirect&code_challenge=${code_challenge}&code_challenge_method=S256&prompt=login&scope=openid+offline+gamesso.token.create+user.profile.read&state=${state}

EOF

    read -p "Response URL: " response_url

    if test -z $response_url; then
        printf "%s\n" "[ERR] response url is empty"
        exit 1
    fi

    code_input=$(echo $response_url | grep -Eo 'code=[^&]*' | cut -d '=' -f2)

    if test -z $code_input; then
        printf "%s\n" "[ERR] could not extract \"code\" HTTP GET parameter"
        exit 1
    else
        printf "%s\n" "[INFO] found code_input: ${code_input}"
    fi

    jwt_url=$(curl -s -X POST -d "grant_type=authorization_code&client_id=com_jagex_auth_desktop_launcher&code=$code_input&code_verifier=$code_verifier&redirect_uri=https://secure.runescape.com/m=weblogin/launcher-redirect" https://account.jagex.com/oauth2/token)
    jwt=$(echo "$jwt_url" | jq -r '.id_token')

    if [ "$jwt" == "null" ]; then
        printf "%s\n" "[ERR] could not extract id token from jwt"
        exit 1
    else
        printf "%s\n" "[INFO] found jwt: ${jwt}"
    fi

    cat << EOF

[INFO] Browse to this URL, wait for redirect to localhost and paste the resulting URL

https://account.jagex.com/oauth2/auth?id_token_hint=${jwt}&nonce=${nonce}&prompt=consent&redirect_uri=http%3A%2F%2Flocalhost&response_type=id_token+code&state=${state}&client_id=1fddee4e-b100-4f4e-b2b0-097f9088f9d2&scope=openid+offline

EOF

    read -p "localhost redirected url: " id_token_url

    if test -z $id_token_url; then
        printf "%s\n" "[ERR] localhost redirected url is empty"
        exit 1
    fi

    id_token=$(echo "$id_token_url" | cut -d'&' -f2 | cut -d'=' -f2)

    if test -z $id_token; then
        printf "%s\n" "[ERR] could not extract \"id_token\" HTTP GET parameter"
        exit 1
    else
        printf "%s\n" "[INFO] found id_token: ${id_token}"
    fi

    sessionIdJson=$(curl -s -X POST -d "{\"idToken\":\"$id_token\"}" -H "Content-Type: application/json" https://auth.jagex.com/game-session/v1/sessions)
    sessionId=$(echo $sessionIdJson | jq -r .sessionId)

    if test -z $sessionId; then
        printf "%s\n" "[ERR] could not extract sessionId"
        exit 1
    else
        printf "%s\n" "[INFO] found sessionId: ${sessionId}"
    fi

    accountIdJson=$(curl -s -H "Authorization: Bearer $sessionId" https://auth.jagex.com/game-session/v1/accounts)
    accountId=$(echo $accountIdJson | jq -r .[0].accountId)
    displayName=$(echo $accountIdJson | jq -r .[0].displayName)

    if test -z $accountId; then
        printf "%s\n" "[ERR] could not extract accountId"
        exit 1
    else
        printf "%s\n" "[INFO] found accountId: ${accountId}"
    fi

    if test -z $displayName; then
        printf "%s\n" "[ERR] could not extract displayName"
        exit 1
    else
        printf "%s\n" "[INFO] found displayName: ${displayName}"
    fi

    cat << EOF

credential.properties:

JX_SESSION_ID=${sessionId}
JX_CHARACTER_ID=${accountId}
JX_DISPLAY_NAME=${displayName}
JX_REFRESH_TOKEN=
JX_ACCESS_TOKEN=
EOF
}

while getopts "dah" opts; do
    case $opts in 
        d) action="download";;
        a) action="authenticate";;
        h) usage;;
        *) usage;;
    esac
done

if [[ $action == "download" ]]; then
    download
elif [[ $action = "authenticate" ]]; then
    authenticate
else
    usage
fi

#!/bin/bash
# -*- coding: utf-8 -*-
export DEBIAN_FRONTEND=noninteractive

trap ctrl_c INT
function ctrl_c() { exit 255; }
function install_dependencies() {
    if [[ $(cat /tmp/.installer-deps) != 1 ]]; then
        sudo apt update -yqq && sudo apt install -yqq \
            curl libc++1 libgconf-2-4 gpg apt-transport-https \
            lsb-release ca-certificates software-properties-common \
            libxcb-xtest0 ibus libgl1-mesa-glx \
            libappindicator1 build-essential libgtkmm-3.0-1v5
        printf "%s\n" "1" > /tmp/.installer-deps
    else
        printf "%s\n" "dependencies already installed"
    fi
}

function _dcss() {
    mkdir -p "${HOME}/.crawl" &>/dev/null
    url=$(curl -sL "https://api.github.com/repos/crawl/crawl/releases" | awk -F\" 'BEGIN {IGNORECASE = 1} /browser_download_url.*linux.*tiles.*64.*AppImage.*/{print $(NF-1)}' | head -1)
    (cd "${HOME}/.crawl"; curl -sL $url -o dcss.AppImage; chmod +x dcss.AppImage; cd -)
    cat > "${HOME}/.crawl/init.txt" << EOF
tile_full_screen = true
tile_cell_pixels = 48
tile_font_crt_size = 20
tile_font_stat_size = 20
tile_font_msg_size = 20
tile_font_tip_size = 20
tile_font_lbl_size = 20
tile_map_pixels = 0
tile_filter_scaling = true 
hp_colour = 100:yellow, 50:red
sort_menus = inv: true : equipped, identified, basename, qualname, charged
explore_auto_rest = true
EOF
    cat > "${HOME}/Desktop/DCSS" << "EOF"
#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Terminal=false
StartupNotify=true
Type=Application
Name=Dungeon Crawl Stone Stoup
Exec=$HOME/.crawl/dcss.AppImage
EOF
}

function _runescape() {
    temp=$(mktemp -d)
    mkdir -p "${HOME}/.wine_runescape/.wine"
    curl -sSL "https://github.com/GloriousEggroll/wine-ge-custom/releases/download/GE-Proton8-9/wine-lutris-GE-Proton8-9-x86_64.tar.xz" -o "${temp}/wine-lutris-GE-Proton8-9-x86_64.tar.xz"
    tar -C "${HOME}/.wine_runescape" -xf "${temp}/wine-lutris-GE-Proton8-9-x86_64.tar.xz"
    rm -rf $temp

    (cd "${HOME}/.wine_runescape"; curl -sSL https://cdn.jagex.com/Jagex%20Launcher%20Installer.exe -o jagex_installer.exe; cd -)
    wine_bin=$(find ~/.wine_runescape/ -type f -name "wine")
    cat > "${HOME}/.wine_runescape/jagex_launcher.sh" << EOF
#!/bin/bash
WINEPREFIX="\${HOME}/.wine_runescape/.wine" WINEDLLOVERRIDES="jscript.dll=n" $wine_bin "\${HOME}/.wine_runescape/.wine/drive_c/Program Files (x86)/Jagex Launcher/JagexLauncher.exe"
EOF
    chmod +x "${HOME}/.wine_runescape/jagex_launcher.sh"
    WINEPREFIX="${HOME}/.wine_runescape/.wine" WINEDLLOVERRIDES="jscript.dll=n" $wine_bin "${HOME}/.wine_runescape/jagex_installer.exe"
}

function _discord() {
    install_dependencies
    curl -sSL 'https://discord.com/api/download?platform=linux&format=deb' -o /tmp/discord.deb
    sudo dpkg -i /tmp/discord.deb
    rm -rf /tmp/discord.deb

    mkdir -p $HOME/.local/bin
    tee $HOME/.local/bin/update_discord << "EOF"
#!/bin/bash
# -*- coding: utf-8 -*-

trap "{ rm -f tmp/discord.deb; }" EXIT
curl -sSL 'https://discord.com/api/download?platform=linux&format=deb' -o /tmp/discord.deb
sudo dpkg -i /tmp/discord.deb
rm -rf /tmp/discord.deb
EOF
    chmod +x $HOME/.local/bin/update_discord
}

function _electrum() {
    install_dependencies
    mkdir -p $HOME/.local/bin
    cd $HOME/.local/bin
    curl -sSL $(curl -sSL https://electrum.org/panel-download.html | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*" | grep -E 'https://.*.AppImage$') -O -J
    chmod +x electrum*
}

function _browsers() {
    install_dependencies
    distro=$(if echo " una bookworm vanessa focal jammy bullseye vera uma " | grep -q " $(lsb_release -sc) "; then echo $(lsb_release -sc); else echo focal; fi)
    rand=$(tr -cd "a-z0-9" </dev/urandom | fold -w8 | head -1)
    mkdir -p "${HOME}/.chrome-profiles" "${HOME}/local/share/icons" "${HOME}/.local/share/applications" "${HOME}/.local/chromium"

    curl -sSL https://dl.google.com/linux/linux_signing_key.pub | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/google-signing-key.gpg
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/google-signing-key.gpg] https://dl.google.com/linux/chrome/deb stable main" > /etc/apt/sources.list.d/google-chrome.list'
    sudo apt update -yqq && sudo apt install -yqq google-chrome-stable

    latest_ungoogled_chromium=$(curl -sSkL https://api.github.com/repos/ungoogled-software/ungoogled-chromium-portablelinux/releases | awk -F '"' '/browser_download_url/ {print $4}' | grep -i '.*AppImage$' | head -1)
    sudo curl -sSL $latest_ungoogled_chromium -o /usr/bin/ungoogled-chromium.AppImage
    sudo chmod +x /usr/bin/ungoogled-chromium.AppImage

    cat << EOF | sudo tee /usr/share/applications/ungoogled-chromium.desktop
[Desktop Action new-private-window]
Exec=/usr/bin/ungoogled-chromium.AppImage --incognito
Name=New Incognito Window

[Desktop Action new-window]
Exec=/usr/bin/ungoogled-chromium.AppImage %U
Name=New Window

[Desktop Entry]
Actions=new-window;new-private-window;
Categories=Network;WebBrowser;
Comment=
Exec=/usr/bin/ungoogled-chromium.AppImage
GenericName=
Icon=chromium-browser
MimeType=text/html;image/webp;image/png;image/jpeg;image/gif;application/xml;application/xml;application/xhtml+xml;application/rss+xml;application/rdf+xml;application/pdf;
Name=Ungoogled Chromium
Path=
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
Version=1.0
EOF
}

function _insomnia() {
    install_dependencies
    INSOMNIA_DEB=$(curl -skL https://api.github.com/repos/Kong/insomnia/releases | grep '.deb' | grep 'browser_download_url' | grep -v 'alpha' | head -1 | grep -Eo 'https://.*.deb')
    curl -skL $INSOMNIA_DEB -o /tmp/insomnia.deb
    sudo dpkg -i /tmp/insomnia.deb
    rm -rf /tmp/insomnia.deb
}

function _obsidian() {
    install_dependencies
    tmp=$(mktemp -d)
    latest_deb=$(curl -sSL https://api.github.com/repos/obsidianmd/obsidian-releases/releases | grep -i 'browser_download_url' | grep -v 'arm' | grep -Eoi 'https://.*_amd64\.deb' | head -1)
    printf "${latest_deb}\n"
    curl -sSL $latest_deb -o "${tmp}/obsidian.deb"
    sudo dpkg -i "${tmp}/obsidian.deb"
    rm -rf $tmp
}

function _postman() {
    install_dependencies
    tar -C /tmp -xzf <(curl -sSL https://dl.pstmn.io/download/latest/linux64)
    sudo mv /tmp/Postman /opt
    sudo tee /usr/share/applications/postman.desktop << EOF
[Desktop Entry]
GenericName=Postman
Name=Postman
Exec=/opt/Postman/Postman
StartupNotify=true
Type=Application
Terminal=false
Icon=/opt/Postman/app/resources/app/assets/icon.png
Categories=Development
EOF
}

function _powershell() {
    install_dependencies
    curl -sL "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb" -o /tmp/packages-microsoft-prod.deb
    sudo dpkg -i /tmp/packages-microsoft-prod.deb
    sudo apt update -yqq && sudo apt install -yqq powershell
    rm /tmp/packages-microsoft-prod.deb
}

function _sf-fonts() {
    install_dependencies
    tmpdir=$(mktemp -d)
    cd $tmpdir
    mkdir -p $HOME/.fonts

    dmgs=(
        "https://devimages-cdn.apple.com/design/resources/download/SF-Pro.dmg"
        "https://devimages-cdn.apple.com/design/resources/download/SF-Compact.dmg"
        "https://devimages-cdn.apple.com/design/resources/download/SF-Mono.dmg"
        "https://devimages-cdn.apple.com/design/resources/download/NY.dmg"
    )
    for dmg in "${dmgs[@]}"; do
        name=$(basename "${dmg}")
        printf "Downloading $dmg\n"
        curl -sSkL $dmg -o $tmpdir/$name
    done

    printf "Unpacking Apple .dmg files...\n"
    for each in *.dmg; do 7z e $each "*.pkg" -r >/dev/null 2>&1; done
    for each in *.pkg; do 7z x "$each" -o"${each}.d" >/dev/null 2>&1; done
    for each in *.d; do 7z x "${each}/*" >/dev/null 2>&1; done

    printf "Installing SF fonts user and system-wide\n"
    cp -r Library/Fonts/*.otf $HOME/.fonts
    sudo mkdir -p /usr/share/fonts/opentype/SF && sudo cp -r Library/Fonts/*.otf /usr/share/fonts/opentype/SF

    printf "Rebuilding font cache\n"
    sudo fc-cache -f
}

function _skype() {
    install_dependencies
    curl -sSL https://repo.skype.com/data/SKYPE-GPG-KEY | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/skype.gpg
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/skype.gpg] https://repo.skype.com/deb stable main" > /etc/apt/sources.list.d/skype-stable.list'
    sudo apt update -yqq && sudo apt install -yqq skypeforlinux
}

function _slack() {
    install_dependencies
    curl -sSL $(curl -sSL https://slack.com/downloads/instructions/ubuntu | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*" | grep -Eo 'https://.*\.deb') -o /tmp/slack.deb
    sudo dpkg -i /tmp/slack.deb
    rm -rf /tmp/slack.deb
}

function _spotify() {
    install_dependencies
    curl -sSL https://download.spotify.com/debian/pubkey_7A3A762FAFD4A51F.gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/spotify.gpg
    sudo sh -c 'echo "deb [signed-by=/etc/apt/trusted.gpg.d/spotify.gpg] http://repository.spotify.com stable non-free" > /etc/apt/sources.list.d/spotify.list'
    sudo apt update -yqq && sudo apt install -yqq spotify-client
}

function _sublime() {
    install_dependencies
    curl -sSL https://download.sublimetext.com/sublimehq-pub.gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/sublimehq-archive.gpg
    sudo sh -c 'echo "deb https://download.sublimetext.com/ apt/stable/" > /etc/apt/sources.list.d/sublime-text.list'
    sudo apt update -yqq && sudo apt install -yqq sublime-text
}

function _vmware() {
    install_dependencies
    version_16=(
        ZF3R0-FHED2-M80TY-8QYGC-NPKYF
        YF390-0HF8P-M81RQ-2DXQE-M2UT6
        ZF71R-DMX85-08DQY-8YMNC-PPHV8
        AZ3E8-DCD8J-0842Z-N6NZE-XPKYF
        FC11K-00DE0-0800Z-04Z5E-MC8T6
    )

    version_17=(
        MC60H-DWHD5-H80U9-6V85M-8280D
        4A4RR-813DK-M81A9-4U35H-06KND
        NZ4RR-FTK5H-H81C1-Q30QH-1V2LA
        JU090-6039P-08409-8J0QH-2YR7F
        4Y09U-AJK97-089Z0-A3054-83KLA
        4C21U-2KK9Q-M8130-4V2QH-CF810
    )

    curl -sSL -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36." https://www.vmware.com/go/getworkstation-linux -o /tmp/vmware-ws-pro.bundle
    sudo bash /tmp/vmware-ws-pro.bundle
    rm /tmp/vmware-ws-pro.bundle

    version=$(vmware -v | grep -Eo "([0-9]{1,}\.)+[0-9]{1,}")
    case "${version}" in
        16*) random_key=${version_16[$RANDOM%${#version_16[@]}]}
             sudo /usr/lib/vmware/bin/licenseTool enter $random_key "" "" $version "VMware Workstation" /usr/lib/vmware
             ;;
        17*) random_key=${version_17[$RANDOM%${#version_17[@]}]}
             sudo /usr/lib/vmware/bin/licenseTool enter $random_key "" "" $version "VMware Workstation" /usr/lib/vmware
             ;;
    esac
}

function _virtualbox() {
    install_dependencies
    LATEST_STABLE=$(curl -sSL https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT)
    URL=$(curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}")
    . /etc/os-release
    UBUNTU_DEB=$(echo "$URL" | grep -i "${UBUNTU_CODENAME}" | head -1 | grep -Eoi "\".*_amd64.deb\"" | tr -d '"')
    curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}/${UBUNTU_DEB}" -o /tmp/virtualbox_amd64.deb
    sudo dpkg -i /tmp/virtualbox_amd64.deb
    rm -rf /tmp/virtualbox_amd64.deb

    EXT_PACK=$(echo "$URL" | grep -Eoi "\".*.vbox-extpack\"" | head -1 | tr -d '"') 
    curl -sSL "https://download.virtualbox.org/virtualbox/${LATEST_STABLE}/${EXT_PACK}" -o "/tmp/${EXT_PACK}"
    echo "yes" | sudo VBoxManage extpack install "/tmp/${EXT_PACK}"
    rm -rf "/tmp/${EXT_PACK}"
}

function _vscode() {
    install_dependencies
    curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/vscode.gpg
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/vscode.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
    sudo apt update -yqq && sudo apt install -yqq code

    mkdir -p $HOME/.config/Code/User
    cat << EOF | tee $HOME/.config/Code/User/settings.json >/dev/null 2>&1
{
    "workbench.startupEditor": "none",
    "workbench.enableExperiments": false,
    "update.mode": "manual",
    "editor.cursorBlinking": "smooth",
    "editor.cursorStyle": "underline",
    "editor.minimap.enabled": false,
    "editor.acceptSuggestionOnCommitCharacter": false,
    "editor.acceptSuggestionOnEnter": "off",
    "workbench.commandPalette.history": 0,
    "workbench.editor.restoreViewState": false,
    "workbench.settings.enableNaturalLanguageSearch": false,
    "window.menuBarVisibility": "toggle",
    "window.restoreWindows": "none",
    "terminal.integrated.cursorStyle": "underline",
    "terminal.integrated.cursorBlinking": true,
    "telemetry.enableCrashReporter": false,
    "update.showReleaseNotes": false,
    "telemetry.enableTelemetry": false,
    "npm.fetchOnlinePackageInfo": false,
    "workbench.sideBar.location": "right",
    "editor.suggestSelection": "first",
    "vsintellicode.modify.editor.suggestSelection": "automaticallyOverrodeDefaultValue",
    "shellcheck.disableVersionCheck": true,
    "workbench.colorTheme": "Breeze Dark",
    "extensions.ignoreRecommendations": true,
}
EOF
}

function _zoom() {
    install_dependencies
    curl -sSL https://zoom.us/client/latest/zoom_amd64.deb -o /tmp/zoom.deb
    sudo dpkg -i /tmp/zoom.deb
    rm -rf /tmp/zoom.deb
}

function _docker() {
    install_dependencies
    curl -fsSL https://get.docker.com -o - | sudo sh
    curl -sSkL $(curl -sKL https://api.github.com/repos/docker/compose/releases | grep -i "$(uname -s)-$(uname -m)" | grep -i 'browser_download_url' | grep -v 'sha256' | head -1 | awk -F'"' '{print $4}') -o - | sudo tee /usr/local/bin/docker-compose >/dev/null 2>&1
    sudo chmod +x /usr/local/bin/docker-compose
    sudo usermod -aG docker $USER
}

function _golang() {
    install_dependencies
    sudo rm -rf /usr/local/go
    tmp=$(mktemp -d)
    URL="https://golang.org/dl/$(curl -sSL https://golang.org/dl | awk -F '"' '/dl\/.*linux-amd64.*tar.gz/{print $(NF-1)}' | awk -F '/' '{print $3}' | head -1)"
    tar -C $tmp -xzf <(curl -sSL "${URL}")
    sudo mv $tmp/go /usr/local/go
}

function _tor-browser-bundle() {
    install_dependencies
    mkdir -p "${HOME}/.tbb" 
    URL="https://torproject.org/$(curl -sSL https://www.torproject.org/download | grep -Eo "[a-zA-Z0-9./?=_%:-]*" | grep -Eo "/dist.*tor.*browser.*.tar.xz$" | head -1)"
    tar -C "${HOME}/.tbb" --strip-components=1 -xJf <(curl -sSL "https://torproject.org/${URL}")
}

function _jetbrains-toolbox() {
    install_dependencies
    mkdir -p "${HOME}/.local/bin"
    tar -C "${HOME}/.local/bin" -xzf <(curl -sSL 'https://download.jetbrains.com/product?code=tb&latest&distribution=linux')
    chmod +x "${HOME}/.local/bin/jetbrains-toolbox"
}

function _kde-utils() {
    install_dependencies
    if [[ $XDG_DATA_DIRS = *plasma* ]] && [[ $(printf $XDG_SESSION_DESKTOP | tr '[[:upper:]]' '[[:lower:]]') = *kde* ]]; then
        sudo apt update -yqq && \
            sudo apt install -yqq kompare okteta ark filelight kfind kwrite sweeper kdiff3 partitionmanager okular 
    else
        printf "%s\n" "desktop not kde, exiting"
        return 255
    fi
}

function _ms-edge() {
    install_dependencies
    curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/microsoft-edge.gpg
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/etc/apt/trusted.gpg.d/microsoft-edge.gpg] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge.list'
    sudo apt update -yqq && sudo apt install -yqq microsoft-edge-beta
}

installers=($(typeset -f | awk '/ \(\) $/ && !/^main / {print $1}' | tr -d '_' | grep -iv ctrlc | grep -iv installdependencies))

if [[ ! $(awk -F= '/^NAME/{print $2}' /etc/os-release | tr -d '"' | tr '[[:upper:]]' '[[:lower:]]') == ubuntu ]]; then
    printf "WARNING: Distribution is not Ubuntu or Ubuntu-based, some packages might not install correctly\n\n"
fi

if [[ "$#" -lt 1 ]]; then
    cat << EOF
Available installers
--------------------
$(for i in "${installers[@]}"; do printf "[*] $i\n"; done)
EOF
else
    for arg in "$@"; do
        if [[ $(type -t "_${arg}") == function ]]; then
            set -x 
            _$arg
            set +x
        fi
    done
fi

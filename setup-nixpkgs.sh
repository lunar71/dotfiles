#!/bin/bash
# -*- coding: utf-8 -*-
set -e

if test $EUID -eq 0; then
    if test -n "$SUDO_USER"; then
        printf "%s\n" "running with sudo as $SUDO_USER"
    else
        printf "%s\n" "run with sudo, not root"
        exit 1
    fi
else
    printf "%s\n" "run with sudo"
    exit 1
fi

export DEBIAN_FRONTEND=noninteractive

#apt-get update -yqq
#apt-get install -yqq \
#    curl \
#    libc++1 \
#    libgconf-2-4 \
#    gpg \
#    apt-transport-https \
#    lsb-release \
#    ca-certificates \
#    software-properties-common \
#    libxcb-xtest0 \
#    ibus \
#    libgl1-mesa-glx \
#    libappindicator1 \
#    build-essential \
#    libgtkmm-3.0-1v5
#
#if [[ $XDG_DATA_DIRS = *plasma* ]] && [[ $(printf $XDG_SESSION_DESKTOP | tr '[[:upper:]]' '[[:lower:]]') = *kde* ]]; then
#    apt-get update -yqq
#    apt-get install -yqq \
#        kompare \
#        okteta \
#        ark \
#        filelight \
#        kfind \
#        kwrite \
#        sweeper \
#        kdiff3 \
#        partitionmanager \
#        okular \
#        kolourpaint
#fi
#
#curl -sSL https://nixos.org/nix/install -o /tmp/nix_install.sh
#/bin/bash /tmp/nix_install.sh --daemon --yes
#rm /tmp/nix_install.sh
#
#cat >> /etc/nix/nix.conf << EOF
#max-jobs = auto
#sandbox = false
#EOF
#systemctl restart nix-daemon
#
#hash -r 

sudo -u "${SUDO_USER}" /bin/bash -l -c '
mkdir -p "${HOME}/.config/nixpkgs"
cat > "${HOME}/.config/nixpkgs/config.nix" << EOF
{ allowUnfree = true; }
EOF'

#nixpkgs="nixpkgs.brave nixpkgs.firefox nixpkgs.ungoogled-chromium nixpkgs.virtualbox nixpkgs.virtualboxExtpack nixpkgs.vmware-workstation nixpkgs.docker nixpkgs.docker-compose nixpkgs.vscode nixpkgs.sublime4"
nixpkgs="nixpkgs.vmware-workstation"

for i in $nixpkgs; do
    sudo -u "${SUDO_USER}" /bin/bash -l -c "nix-env -iA ${i}"
done

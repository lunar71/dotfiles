#!/bin/bash
# -*- coding: utf-8 -*-

[[ ! $(command -v ovftool) ]] && printf "%s\n" "ovftool not found, exiting" && exit 1

usage() {
    cat << EOF
Usage: $(basename $0) -o <ova> -u <username> -p <password> -s <server> [-d <datastore>] [-n <network name>]

-o      </path/to/vm.ova>
-u      <username>
-p      <password>
-s      <esxi server>
-d      optional datastore name, default datastore03
-n      optional network name, default "VM Network"
EOF
}

urlencode() {
    local string="${1}"
    local length="${#string}"
    local encoded=""
    for (( i = 0; i < length; i++ )); do
    local char="${string:i:1}"
    if [[ "${char}" =~ [A-Za-z0-9] ]]; then
      encoded+="${char}"
    else
      encoded+=$(printf '%%%02X' "'${char}")
    fi
    done
    printf "%s" "${encoded}"
}

while getopts "o:u:p:s:d:n:" ARG; do
    case "${ARG}" in
        o) ova="${OPTARG}";;
        u) username="${OPTARG}";;
        p) password="${OPTARG}";;
        s) server="${OPTARG}";;
        d) datastore_name="${OPTARG}";;
        n) network_name="${OPTARG}";;
        h) usage;;
        *) usage;;
    esac
done

if [[ -z "${ova}" ]] || [[ -z "${username}" ]] || [[ -z "${password}" ]] || [[ -z "${server}" ]]; then
    usage
fi

if [[ ! -f "${ova}" ]]; then
    printf "%s\n" "[-] ${ova} is not a file, exiting"
    exit 1
fi

urlencoded_password=$(urlencode "${password}")

[[ -z $datastore_name ]] && datastore_name="datastore03"
[[ -z $network_name ]] && network_name="VM Network"

printf "%s\n\n" "[*] Deploying ${ova} to ${server}"
ovftool -ds="${datastore_name}" -nw="${network_name}" -n="newvm" $ova "vi://${username}:${urlencoded_password}@${server}"

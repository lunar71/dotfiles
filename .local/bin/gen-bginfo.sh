#!/bin/bash

usage() {
    printf "%s\n" \
        "usage: $(basename $0) [-t <text>] [-h] [-n] [-p <position>]" \
        "" \
        "-t     text for wallpaper" \
        "-h     include hostname" \
        "-n     include network info" \
        "-p     position: n, ne, e, se, s, sw, w, nw, c"
    exit 1
}

if ! command -v convert &>/dev/null; then
    printf "%s\n" "[ERR] imagemagick (convert) not found"
    exit 1
fi

while getopts "t:hnp:" opts; do
    case $opts in
        t) text="${OPTARG}";;
        h) hostname=true;;
        n) network=true;;
        p) case "${OPTARG}" in
            n)  pos="north";;
            ne) pos="northeast";;
            e)  pos="east";;
            se) pos="southeast";;
            s)  pos="south";;
            sw) pos="southwest";;
            w)  pos="west";;
            nw) pos="northwest";;
            c)  pos="center";;
            *)  usage;;
        esac
    esac
done

if ! test "${text}" && ! test "${hostname}" && ! test "${network}"; then
    usage
fi

if ! test "${pos}"; then
    usage
fi

if test "${pos}" == "c"; then
    offset="+0+0"
else
    offset="+50+0"
fi

if test "${text}"; then
    annotation+="\n${text}"
fi

if test "${hostname}" == "true"; then
    annotation+="\n$(hostname)"
fi

if test "${network}" == "true"; then
    iface=$(ip r get 8.8.8.8 | grep -oP "dev \K[^ ]+")
    ip=$(ip r get 8.8.8.8 | grep -oP "src \K[^ ]+")
    annotation+="\n${ip} (${iface})"
fi

convert \
    -size 1920x1080 \
    canvas:'#2f2f2f' \
    -font Noto-Sans-Mono-Light \
    -gravity "${pos}" \
    -pointsize 32 \
    -fill white \
    -annotate "${offset}" \
    "${annotation}" wallpaper.jpg

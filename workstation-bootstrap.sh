#!/bin/bash
# -*- coding: utf-8 -*-

# keepass backup as cronjob (escape %)
# SHELL=/bin/bash
# PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
# */30 * * * * [[ $(command -v smbclient) ]] && [[ ! $(pgrep keepass*) ]] && smbclient -U 'backup\%<hash>' --pw-nt-hash //server/share -c "mkdir keepassx; put $HOME/keepassx/keepassx.kdbx keepassx/keepassx.kdbx"
#
# */30 * * * * [[ $(command -v smbclient) ]] && [[ $(command -v gpg) ]] && [[ ! $(pgrep keepass*) ]] && gpg --symmetric --output $HOME/test.gpg --batch --yes --passphrase "test123" $HOHE/.local/keepassx/keepassx.kdbx smbclient -U 'backup\%<hash>' --pw-nt-hash //server/share -c "mkdir keepassx; put $HOME/test.gpg keepassx/keepassx.kdbx"

#if [[ -d .git ]]; then
#    [[ ! -z $(git status --untracked-files=no --porcelain) ]] && printf "repository doesn't seem to be latest, do a git pull\n"&& exit 5
#else
#    printf "This isn't a git repo, clone the repo first\n"
#    exit 5
#fi

sudo x >/dev/null 2>&1
set -x
set -e 

DEBIAN_FRONTEND=noninteractive

mkdir -p $HOME/{.local/bin,.ssh/config.d,.fonts}
cp .bashrc "${HOME}/.bashrc"
cp .xbindkeysrc "${HOME}/.xbindkeysrc"
cp .tmux.conf "${HOME}/.tmux.conf"
cp -r ".local/" "${HOME}"

# kolourpaint for kde
# packages and tools
base_tools=(
    apt-transport-https
    wget
    curl
    jq
    gpg
    tmux
    vim
    git
    ufw
    openvpn
    xclip
    keepassxc
    remmina
    tree
    ethtool
    xbindkeys
    xautomation
)
sudo apt update -yqq
sudo apt install -yqq "${base_tools[@]}"

sudo apt autoremove --purge -yqq cups
sudo ufw allow 22/tcp && sudo ufw enable && sudo systemctl enable ufw

# git
git config --global http.sslVerify "false"
git config --global credential.helper "cache --timeout=3600"

# vim
[[ ! -d "${HOME}/.vim/pack/plugins/start/lightline" ]] && git clone https://github.com/itchyny/lightline.vim "${HOME}/.vim/pack/plugins/start/lightline"
[[ ! -d "${HOME}/.vim/colors" ]] && git clone https://github.com/utensils/colors.vim "${HOME}/.vim/colors"

cat > "${HOME}/.vimrc" << EOF
syntax enable
filetype indent on 
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set fileformat=unix
set laststatus=2
set number
set showmatch
set ignorecase
set encoding=utf-8
scriptencoding utf-8
set background=dark
colo base16-google-dark
highlight LineNr ctermbg=black
let python_highlight_all = 1
let g:lightline = {
      \ 'colorscheme': 'powerline',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'readonly', 'filename', 'modified', 'charvaluehex' ] ]
      \ },
      \ 'component': {
      \   'charvaluehex': '0x%B'
      \ },
      \ }
inoremap jk <esc>
nnoremap tg gT
nnoremap B ^
nnoremap E $
nnoremap $ <nop>
noremap ^ <nop>
nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>
set noswapfile
EOF

# ssh
[[ ! -f "${HOME}/.ssh/config" ]] && touch "${HOME}/.ssh/config"
if [[ ! $(grep -Pzo "^Host \*\nServerAliveInterval 60\nServerAliveCountMax 2" "${HOME}/.ssh/config") ]]; then
    cat > "${HOME}/.ssh/config" << EOF
Host *
    ServerAliveInterval 60
    ServerAliveCountMax 2
    User root
EOF
fi

if [[ ! $(grep 'Include config.d' "${HOME}/.ssh/config") ]]; then
    sed -i '1i Include config.d/*' "${HOME}/.ssh/config"
fi

if [[ $DESKTOP_SESSION == *plasma* ]]; then
    cp .kde-settings "${HOME}/.config"
    #bash "${HOME}/.config/.kde-settings"
fi

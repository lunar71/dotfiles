# exports
export VIRTUAL_ENV_DISABLE_PROMPT=1
export PS1="\[\033[90m\]\$(_is_tmux)\[\033[0m\]\[\033[90m\]\$(_tmux_count)\[\033[0m\]\[\033[90m\]\$(_venv)\[\033[0m\]\[\033[91m\]\u\[\033[0m\] \[\033[92m\]\w\[\033[91m\]\$(_git_branch)\[\033[0m\] \$ "
export PATH=$PATH:$HOME/.local/bin:/usr/local/go/bin:$HOME/go:$HOME/go/bin

# utility functions
exit() { [[ -n "${TMUX}" ]] && tmux detach-client || builtin exit; }
_venv() { [[ $VIRTUAL_ENV ]] && printf "($(basename $VIRTUAL_ENV 2>/dev/null)) "; }
_git_branch() { /usr/bin/git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ (git:\1)/'; }
record() { export PS1="\$ "; }
reload() { source ~/.bashrc; }
tmp() { cd $(mktemp -d); }
_tmux_count() {
    count=$(printf "%s\n" "$(tmux list-sessions 2>/dev/null | grep -v '^$' | wc -l)")
    [[ "${count}" -gt 0 && -z "${TMUX}" ]] && printf "(${count} tmux) "
}
_is_tmux() { [[ -n "${TMUX}" ]] && printf "[tmux] "; }

# bash completion functions
_ovpn_completion() {
    local DIR="${HOME}/.ovpn.d"
    [[ ! -d $DIR ]] && mkdir -p $DIR >/dev/null 2>&1
    local PROFILES=$(\ls $DIR | tr ' ' '\n')
    local PATTERN='!*.ovpn'
    local CURSOR="${COMP_WORDS[COMP_CWORD]}"
    local CURSOR_PREV="${COMP_WORDS[COMP_CWORD-1]}"
    local CURSOR_NEXT="${COMP_WORDS[COMP_CWORD+1]}"

    if [[ "${CURSOR_PREV}" == "connect" ]] || [[ "${CURSOR_PREV}" == "delete" ]]; then
        COMPREPLY=($(compgen -f -X "${PATTERN}" -W "${PROFILES}" -- "${CURSOR}"))
    elif [[ "${CURSOR_PREV}" == "install" ]]; then
        COMPREPLY=($(compgen -f -- "${CURSOR}"))
    else
        COMPREPLY=($(compgen -W "connect install delete" -- "${CURSOR}"))
    fi

    return 0
}

complete -o filenames -F _ovpn_completion ovpn

# functions
function ovpn() {
    DIR="${HOME}/.ovpn.d"

    if [[ "$#" -lt 2 ]]; then
        printf "%s\n\n" "Command-line OpenVPN profile manager that supports auto-completion"
        printf "%s\n" "Usage: ${FUNCNAME[0]} [connect|install|delete]"
        printf "%s\n" "${FUNCNAME[0]} connect  <profile.ovpn>      connect using openvpn profile"
        printf "%s\n" "${FUNCNAME[0]} install  <profile.ovpn>      installs openvpn profile to ${DIR}"
        printf "%s\n" "${FUNCNAME[0]} delete   <profile.ovpn>      deletes openvpn profile from ${DIR}"
        return 1
    fi

    OPT=$1
    PROFILE=$2

    case "${OPT}" in
        connect)
            sudo openvpn --cd "${DIR}" --config "${DIR}/${2}"
            ;;
        install)
            if [[ -f "${2}" ]]; then
                install --backup=numbered -o "${USER}" -g "${USER}" -m 600 --preserve-timestamps "${2}" "${DIR}"
                printf "%s\n" "Installed \"${2}\" to ${DIR}"
            elif [[ ! -f "${2}" ]]; then
                printf "%s\n" "\"${2}\" is not a file"
            fi
            ;;
        delete)
            if [[ -f "${DIR}/${2}" ]]; then
                printf "%s\n" "Deleting file \"${2}\" from ${DIR}"
                rm -f "${DIR}/${2}"
            elif [[ ! -f "${DIR}/${2}" ]]; then
                printf "%s\n" "There is no \"${2}\" file in ${DIR}, skipping"
            fi
            ;;
    esac
}

function venv() {
    [[ -z $1 ]] && printf "Create python2 or python3 virtualenv and source it\nUsage: ${FUNCNAME[0]} 2|3\n" && return
    [[ ! $(command -v virtualenv) ]] && printf "virtualenv package not installed\n" && return
    RAND=$(printf $(tr -dc 'a-z0-9' </dev/urandom | head -c 5))
    case $1 in
        "2")
            virtualenv -p python2 "py2venv-$RAND" && source "py2venv-$RAND/bin/activate";;
        "3")
            virtualenv -p python3 "py3venv-$RAND" && source "py3venv-$RAND/bin/activate";;
        *) printf "Unknown option\n";;
    esac
}

function sshtmux() {
    [[ -z $1 ]] && printf "${FUNCNAME[0]} <ssh args>\n" && return
    ssh $@ -t 'IGNOREEOF=10 tmux attach 2>/dev/null || IGNOREEOF=10 tmux 2>/dev/null || printf "tmux not installed, exiting...\n"'
}

function update() {
    set -o functrace
    case "$(. /etc/os-release && printf "${ID}" | tr '[:upper:]' '[:lower:]')" in
        ubuntu|debian|raspbian)
            sudo apt update -y 
            sudo apt upgrade -y
            sudo apt autoremove -y
            sudo apt autoclean -y && sudo apt clean -y
            ;;
        centos|fedora|rhel)
            sudo yum update -y
            sudo yum clean all -y
            ;;
    esac
}

function android-utils() {
    [[ ! $(command -v adb) ]] && printf "adb not found, exiting\n" && return 1
    [[ $# -lt 1 ]] && printf "Usage: ${FUNCNAME[0]} < [logcat <package name>] | packages | ps | push-burp-cert >\n" && return 1
    case $1 in
        logcat)
            shift
            [[ -z $1 ]] && printf "Supply a package name\n" && return 1
            adb logcat --pid=$(adb shell pidof -s $1)
            ;;
        packages)
            adb shell pm list packages | cut -d':' -f2
            ;;
        ps)
            adb shell ps -A
            ;;
        push-burp-cert)
            shift
            set -x
            [[ -z $1 ]] && PROXY='http://127.0.0.1:8080' || PROXY=$1
            curl -sSkL --proxy $PROXY -o /tmp/cacert.der http://burp/cert
            openssl x509 -inform DER -in /tmp/cacert.der -out /tmp/cacert.pem
            cp /tmp/cacert.der $(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0
            adb shell su -c "mount -o remount,rw /system"
            adb push "$(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0" /sdcard/
            adb shell su -c "mv /sdcard/$(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0 /system/etc/security/cacerts/"
            adb shell su -c "chmod 644 /system/etc/security/cacerts/$(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem | head -1).0"
            rm $(openssl x509 -inform PEM -subject_hash_old -in /tmp/cacert.pem |head -1).0
            rm /tmp/cacert.pem
            rm /tmp/cacert.der
            ;;
        pull-apk)
            shift
            [[ -z $1 ]] && printf "Supply a package name to pull APK\n" && return 1
            adb pull $(adb shell pm path $1 | cut -d':' -f2) .
            ;;
    esac
}

function splitter() {
    [[ "$#" -lt 2 ]] && printf "Usage ${FUNCNAME[0]} <file.txt> <parts>\n" && return 1
    file="$1"
    parts="$2"
    filename="${file%.*}"
    extension="${file##*.}"
    total_lines=$(wc -l < "$file")
    lines_per_part=$((total_lines / parts + 1))
    split -l "${lines_per_part}" -d --additional-suffix=".${extension}" "$file" "${filename}_"
}

function killproc() {
    [[ -z $1 ]] && printf "%s\n" \
        "kill all processes by name" \
        "usage: ${FUNCNAME[0]} <process name>" && \
        return 1
    proc="[${$1:0:1}]${$1:1}"
    for i in $(ps aux | grep -i $1 | awk '{print $2}'); do
        printf "%s\n" "killing $i"
        kill -9 $i
    done
}

function ytdl() {
    [[ -z $1 ]] && printf "%s\n" \
        "download youtube audio, best quality, mp3 output" \
        "usage: ${FUNCNAME[0]} <url>" && \
        return 1
    yt-dlp --audio-format mp3 --audio-quality bestaudio -x $1 -o "%(title)s.%(ext)s"
}

# aliases
alias ..='cd ..'
alias ...='cd ../..'
alias cd..='cd ..'
alias cd...='cd ../..'
alias d='cd ~/Downloads'
alias mkdir='mkdir -pv'
alias ll='ls --color=auto -lah'
alias ls='ls --color=auto -lah'
alias cp='cp -r'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias cat='cat -v'
alias vim='vim -p'
alias ip='ip --brief --color'
alias clip='xclip -i -sel p -f | xclip -i -sel c'
alias scrcpy='scrcpy -St'
alias genpasswd='tr -cd "[:alnum:]" </dev/urandom | fold -w32 | head -1'
alias cidrgrep='grep -o "[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\/[0-9]\{1,\}"'
alias kdelock='qdbus org.kde.ksmserver /ScreenSaver org.freedesktop.ScreenSaver.Lock'
